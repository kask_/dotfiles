{
  config,
  nixpkgs,
  pkgs,
  user,
  ...
}: {
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";
  boot.loader.grub.useOSProber = true;

  hardware.bluetooth.enable = true;

  networking.hostName = "laptop";
}
