{pkgs, ...}: {
  home = {
    packages = with pkgs; [
      birdtray
      bitwarden
      element-desktop
      flameshot
      logseq
      qbittorrent
      thunderbird
      vlc
      zoom-us

      # School

      octave
      speedcrunch
    ];
  };

  imports = import ../../modules/internet;
}
