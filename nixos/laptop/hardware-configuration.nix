{
  config,
  lib,
  modulesPath,
  pkgs,
  ...
}: {
  boot = {
    initrd.availableKernelModules = ["ata_piix" "ohci_pci" "sd_mod" "sr_mod"];
    kernelModules = ["kvm-amd"];
  };

fileSystems."/" =
    { device = "/dev/disk/by-uuid/e4686577-7124-48d6-afa4-3c0a27be6831";
      fsType = "ext4";
    };

  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
  virtualisation.virtualbox.guest.enable = true;

  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];
}
