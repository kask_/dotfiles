{pkgs, ...}: {
  programs = {
    git = {
      enable = true;
      userEmail = "isaac_s_brown@protonmail.com";
      userName = "Isaac";
    };
  };
}
